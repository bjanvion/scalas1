package example

object RecFun {

  def main(args: Array[String]) {
   val str = ""
    println("balence "+ balance(str.toList))
    println("countChange "+ countChange(7, List(1,7,2)))

  }

  def balance(chars: List[Char]): Boolean = {
    def countingParenthesis(chars: List[Char], openParenthesis: Int): Boolean = chars match {
      case Nil => println("null");openParenthesis == 0
      case '(' :: cs => countingParenthesis(cs, openParenthesis + 1)
      case ')' :: cs => if (openParenthesis == 0) false else countingParenthesis(cs, openParenthesis - 1)
      case _ :: cs => countingParenthesis(cs, openParenthesis)
    }

    countingParenthesis(chars, 0)
  }
  def countChange(money: Int, coins: List[Int]): Int =
    if (money == 0) 1
    else if (coins.isEmpty) 0
    else if (coins.head <= money) countChange(money - coins.head, coins) + countChange(money, coins.tail)
    else countChange(money, coins.tail)



}
