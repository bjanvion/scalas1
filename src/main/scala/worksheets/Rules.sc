def exampleEvaluatedWhenCalling =2
println(exampleEvaluatedWhenCalling)

val exampleEvaluatedNow =2
println(exampleEvaluatedNow)

lazy val example = 2 // evaluated once when needed
println(example)


def squareByValue (x: Double) = x * x    // call by value
def squareByName(x: => Double)  = x * x // call by name

println(squareByName(3));
println(squareByName(4));


def myFct(bindings: Int*) = {
//  if(bindings.length ==1) bindings.head
 // else bindings.head + myFct(bindings.)
  bindings.sum
}
println(myFct(1,2,3,4,5));