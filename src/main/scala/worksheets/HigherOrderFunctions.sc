
def simpleSum(a: Int , b: Int): Int =   a + b
println(simpleSum(1, 3))


/*
Dans la méthode dite "curry", ce qui signifie qu'au lieu d'avoir une fonction
 qui évalue n arguments,
 vous avez n fonctions évaluant un argument,
     chacune renvoyant une nouvelle fonction jusqu'à ce que vous évaluiez la dernière.
 */
// same as above. Its type is (Int => Int) => (Int, Int) => Int
def sum(f: Int => Int)(a: Int, b: Int): Int = {
   f(a) + (a + b)

}
//println(sum(1)(2,3))


// curriedSum is a function that takes an integer,
// which returns a function that takes an integer
// and returns the sum of the two
def curriedSum(a: Int): Int => Int =
   b => a + b
//curriedSum(2)
val sumTwo: Int => Int = curriedSum(2)
val four = sumTwo(2)

// traitement en 2 fois
def curriedSum2(x:Int) = (y:Int) => x + y
curriedSum2(2)(3)

def curriedSum3(a: Int)(b: Int): Int = a + b
curriedSum3(2)(3)


def summ(f: Int => Int): (Int, Int) => Int = {
   def sumf(a: Int, b: Int): Int = {
      if (a > b) 0
      else f(a)
   }
   sumf
}
summ( 3 => 3)(2, 5)