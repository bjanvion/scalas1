

val liste= List(1,3,2)
//val liste= List(1)
 liste match {
   case Nil => println("vide")
   //case x :: Nil => 22     // list with only one element
   case List(x) => 22      // same as above ( list with only one element)
   //case _ :: tail => 4  // si au moiins 1 elem
   case x :: xs => xs     // a list with at least one element. x is bound to the head,
                          // xs to the tail. xs could be Nil or some other list.
   case _ => "defaut case"            // default case if none of the above matches
 }
