package coursera.devoir.recfun

object RecFun extends RecFunInterface {

  def main(args: Array[String]): Unit = {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(s"${pascal(col, row)} ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if(c==0 || c==r) 1
    else pascal(c, r-1) + pascal(c-1, r-1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    countParentheses(chars,0)
  }


  private def countParentheses(chars: List[Char], openParentheses :Int): Boolean = {
    if (chars.isEmpty) openParentheses == 0
    else {
      if (chars.head.equals('('))
        countParentheses(chars.tail, openParentheses + 1)
      else if (chars.head.equals(')'))
        if (openParentheses == 0) false
        else countParentheses(chars.tail, openParentheses - 1)
      else countParentheses(chars.tail, openParentheses)
    }
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {

    if(money == 0) 1
    else if (money < 0 || coins.isEmpty) 0
    else countChange(money, coins.tail) + countChange(money - coins.head, coins)

  }
}
