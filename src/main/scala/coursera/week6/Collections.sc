val nums = Vector(1, 2,4,5,44)

//All  operation of List with exception :: à la place := ou =:
val num2 = nums :+ 5

//Lists and Vectors are 2 implementation of Seq
// arrays ans String support the sames operations as Seq et can be converted to seq
val numseq = Seq(1,3,4)
numseq :+ 4


val xsArray = Array(1,2,4,55)
xsArray.map(_ *2)

val s = "Hello World"
s.filter(_.isUpper)
s.exists(c=> c.isUpper)
// tous les caracteres sont maj ?
s.forall(c=> c.isUpper)

val pairs = nums.zip(s) //Vector((1,H), (2,e), (4,l), (5,l))
pairs.unzip // (Vector(1, 2, 4, 5),Vector(H, e, l, l))

// combiner la liste et le vector
s.flatMap(c=> List(".*#", c))  //Vector(.*#, H, .*#, e, .*#, l, .*#, l, .*#, o, .*#,  , .*#, W, .*#, o, .*#, r, .*#, l, .*#, d)
nums.sum  // somme des int du seq : 56
nums.max  //44


val r: Range = 1 until(9)
val r1: Range = 1 to 9
1 to 10 by 3
r1(1)


val test = r.exists(_>2)

def scalarProduct (xs : Vector[Double], ys : Vector[Double]) : Double ={
  //xy._1  coté gauche de la pair
  // xy._2 coté droit
  // map application pour chaque elem , à la fin on somme tous les valeurs de la liste de pairs
  xs.zip(ys).map(xy => xy._1 * xy._2).sum
}



def isPrime(n: Int) : Boolean = (2 until n) forall( d=> n%d !=0)
