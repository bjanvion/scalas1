package coursera.week3

trait Liste[T] {
  def isEmpty : Boolean
  def head: T
  def tail: Liste[T]


  def at( n :Int  ): T = {
    if (this.isEmpty) throw new IndexOutOfBoundsException
    else if (n== 0) this.head
    else this.tail.at(n-1)
  }


}


class Cons[T](val head: T, val tail: Liste[T]) extends Liste[T]{
  def isEmpty : Boolean = false


}

class NiL[T]  extends Liste[T]{
  def isEmpty : Boolean = true
  def head: Nothing =  throw new NoSuchElementException("Nil .head")
  def tail: Nothing =  throw new NoSuchElementException("Nil .tail")

}



object Polymorphism extends App {

  def singleton[T] (elem:T) = new Cons[T](elem, new NiL[T])


  def nth[T]( n :Int  , xs: Liste[T]): T = {
      if (xs.isEmpty) throw new IndexOutOfBoundsException
      if (n== 0) xs.head
      else nth[T](n-1, xs.tail)
  }

  val liste = new Cons(1, new Cons(2, new Cons(3, new Cons(4, new NiL))))
  println( "liste element at  2 "+nth(2, liste))
  println( "liste element at  2 by at "+liste.at(1))

  val s =  singleton[Int] (1)
  println( "s element at 0 "+nth(0, s))


  val liste2 = new Cons[Boolean](true, new Cons[Boolean](false, new NiL[Boolean]))
//  val liste3 = new Cons(true, new Cons("string", new NiL))  //  type auto determined : execution error //error type mismatch


  val liste4 = new Cons(true, new Cons(false, new NiL))  //  type auto determined
  println( "liste3 element at  2 "+nth(1, liste4))  //error type mismatch

//  println( "head "+s.head+ "tail "+s.tail)
//  val b =  singleton[Boolean] (true)

}
